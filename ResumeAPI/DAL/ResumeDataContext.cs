namespace ResumeAPI.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ResumeAPI.Models;

    public partial class ResumeDataContext : DbContext
    {
        public ResumeDataContext()
            : base("name=ResumeDataContext")
        {
        }

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Education> Educations { get; set; }
        public virtual DbSet<Employment> Employments { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectSkill> ProjectSkills { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<Volunteer> Volunteers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Education>()
                .Property(e => e.GPA)
                .HasPrecision(3, 2);

            modelBuilder.Entity<Education>()
                .HasMany(e => e.Courses)
                .WithOptional(e => e.Education)
                .HasForeignKey(e => e.SchoolID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Courses)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID);

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Educations)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Employments)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Projects)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Skills)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Volunteers)
                .WithOptional(e => e.Profile)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Project>()
                .HasMany(e => e.ProjectSkills)
                .WithOptional(e => e.Project)
                .WillCascadeOnDelete();
        }
    }
}
