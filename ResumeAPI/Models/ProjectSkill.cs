namespace ResumeAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProjectSkill")]
    public partial class ProjectSkill
    {
        public int ID { get; set; }

        public int? SkillID { get; set; }

        public int? ProjectID { get; set; }

        public virtual Project Project { get; set; }

        public virtual Skill Skill { get; set; }
    }
}
