namespace ResumeAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employment")]
    public partial class Employment
    {
        public int ID { get; set; }

        public int? UserID { get; set; }

        [Required]
        [StringLength(255)]
        public string Company { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(1024)]
        public string Description { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
