namespace ResumeAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Course")]
    public partial class Course
    {
        public int ID { get; set; }

        public int? UserID { get; set; }

        public int? SchoolID { get; set; }

        [Required]
        [StringLength(10)]
        public string CourseNumber { get; set; }

        [Required]
        [StringLength(255)]
        public string CourseTitle { get; set; }

        [Required]
        [StringLength(1024)]
        public string CourseDescription { get; set; }

        [Required]
        [StringLength(2)]
        public string Grade { get; set; }

        public virtual Profile Profile { get; set; }

        public virtual Education Education { get; set; }
    }
}
